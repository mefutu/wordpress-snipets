<?php
// wp-config.php

define( 'UPLOADS', '/app/uploads' );

define( 'WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/app' );
define( 'WP_CONTENT_URL', 'https://site.ru/app' );

define( 'WP_PLUGIN_DIR', $_SERVER['DOCUMENT_ROOT'] . '/app/plugins' );
define( 'WP_PLUGIN_URL', 'https://site.ru/app/plugins');
define( 'PLUGINDIR', $_SERVER['DOCUMENT_ROOT'] . '/app/plugins' );