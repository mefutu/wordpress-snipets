<?php 

add_filter( 'rest_url_prefix', 'my_theme_api_slug'); 
function my_theme_api_slug( $slug ) { return 'api'; }
flush_rewrite_rules(true);
	
add_filter('xmlrpc_enabled', '__return_false');




require_once dirname(__FILE__).'/inc/class-tgm-plugin-activation.php';
add_action('tgmpa_register','my_theme_register_required_plugins');
function my_theme_register_required_plugins(){
	$plugins = array(
		/* установка плагинов из папки темы, которые вы поставляете в комплекте */
		// array(
		// 	'name' => 'TGM Example Plugin', // имя  плагина
		// 	'slug' => 'tgm-example-plugin', // Имя плагина (совпадает с именем папки плагина)
		// 	'source' => get_stylesheet_directory().'/plugins/tgm-example-plugin.zip', // источник плагина
		// 	'required' => true, 
		// 	// 'required' - плагин обязательый? нужно ли го активировать поле установки?
		// ),
		/* установка плагинов на автомате из репозитория wordpress */
		array(
			'name' => 'WP Mail Smtp',
			'slug' => 'wp-mail-smtp',  		
			'required' => true, 
		), 
		array(
			'name' => 'All-in-One WP Migration',
			'slug' => 'all-in-one-wp-migration',  		
			'required' => true, 
		), 
		array(
			'name' => 'WP Multilang',
			'slug' => 'wp-multilang',  		
			'required' => true, 
		), 
		array(
			'name' => 'Jetpack',
			'slug' => 'jetpack',  		
			'required' => true, 
		), 
		array(
			'name' => 'All in One Seo',
			'slug' => 'all-in-one-seo-pack',  		
			'required' => true, 
		), 
		array(
			'name' => 'WooCommerce',
			'slug' => 'woocommerce',  	 
		), 
		array(
			'name' => 'WooCommerce currency switcher',
			'slug' => 'woocommerce-currency-switcher',  	 
		), 
		array(
			'name' => 'Clearfy',
			'slug' => 'clearfy',  	 	
			'required' => true, 
		), 
		array(
			'name' => 'WP PageNavi',
			'slug' => 'wp-pagenavi',  	 	
			'required' => true, 
		), 
		array(
			'name' => 'Login LockDown',
			'slug' => 'login-lockdown',  	 	
			'required' => true, 
		), 
		array(
			'name' => 'Rest API Blocker',
			'slug' => 'disable-json-api',  	 	
			'required' => true, 
		), 
		array(
			'name' => 'Theme Check',
			'slug' => 'theme-check',  	 	
			'required' => true, 
		), 
		array(
			'name' => 'ACF to REST API',
			'slug' => 'acf-to-rest-api', 
			'required' => true, 
		), 
		array(
			'name' => 'REST API Toolbox',
			'slug' => 'rest-api-toolbox', 
			'required' => true, 
		), 
		array(
			'name' => 'Breadcrumb NavXT',
			'slug' => 'breadcrumb-navxt', 
			'required' => true, 
		), 
		array(
			'name' => 'WP user AVATAR',
			'slug' => 'wp-user-avatar', 
			'required' => true, 
		), 
		array(
			'name' => 'PublishPress Capabilities',
			'slug' => 'capability-manager-enhanced', 
			'required' => true, 
		), 
		
	);
	
	$theme_text_domain = 'twentytwenty'; // текстовый домен темы
	$config = array(
		/*domain => $theme_text_domain, // текстовый домен, точно такой как указан в вашей теме*/
		/*dafault_path => '', // Абсолютный путь по умолчанию к папке плагинов*/
		/*menu => 'install-my-theme-plugin', // Menu slug*/
		'settings' => array(
			/*'page_title'	=> __('Install Required Plugins', $theme_text_domain)*/
			/*'menu_title'	=> __('Install Plugins', $theme_text_domain)*/
			/*'instructions_install'	=> __( 'The %1$s plugin is required for this theme. Click on the big blue button below to install and activate %1$s.', $theme_text_domain ), // %1$s = plugin name */
			/*'instructions_activate'  => __( 'The %1$s is installed but currently inactive. Please go to the <a href="%2$s">plugin administration page</a> page to activate it.', $theme_text_domain ), // %1$s = plugin name, %2$s = plugins page URL */
			/*'button'                 => __( 'Install %s Now', $theme_text_domain ), // %1$s = plugin name */
			/*'installing'             => __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name */
			/*'oops'                   => __( 'Something went wrong with the plugin API.', $theme_text_domain ), // */
			/*'notice_can_install'     => __( 'This theme requires the %1$s plugin. <a href="%2$s"><strong>Click here to begin the installation process</strong></a>. You may be asked for FTP credentials based on your server setup.', $theme_text_domain ), // %1$s = plugin name, %2$s = TGMPA page URL */
			/*'notice_cannot_install'  => __( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', $theme_text_domain ), // %1$s = plugin name */
			/*'notice_can_activate'    => __( 'This theme requires the %1$s plugin. That plugin is currently inactive, so please go to the <a href="%2$s">plugin administration page</a> to activate it.', $theme_text_domain ), // %1$s = plugin name, %2$s = plugins page URL */
			/*'notice_cannot_activate' => __( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', $theme_text_domain ), // %1$s = plugin name */
			/*'return'                 => __( 'Return to Required Plugins Installer', $theme_text_domain ), // */
		),
	);
	tgmpa( $plugins, $config );
}