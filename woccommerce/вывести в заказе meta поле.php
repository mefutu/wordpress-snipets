<?php 

// Output a custom editable field in backend edit order pages under general section
add_action( 'woocommerce_admin_order_data_after_order_details', 'editable_order_custom_field', 12, 1 );
function editable_order_custom_field( $order ){
    // Loop through order items
    foreach( $order->get_items() as $item_id => $item ){
        // Get "customer reference" from order item meta data
        if( $item->get_meta('Your Reference') ){
            // The "customer reference"
            $item_value = $item->get_meta('Your Reference');

            // We output a hidden field with the Item ID (to be able to update this order item meta data later)
            echo '<input type="hidden" name="item_id_ref" value="' . $item_id . '">';

            break; // We stop the loop
        }
    }

    // Get "customer reference" from meta data (not item meta data)
    $updated_value = $order->get_meta('META_FIELD');

    // Replace "customer reference" value by the meta data if it exist
    $value = $updated_value ? $updated_value : ( isset($item_value) ? $item_value : '');

    // Display the custom editable field
    woocommerce_wp_text_input( array(
        'id'            => 'customer_ref',
        'label'         => __("Customer Reference:", "woocommerce"),
        'value'         => $value,
        'wrapper_class' => 'form-field-wide',
    ) );
}

// Save the custom editable field value as order meta data and update order item meta data
add_action( 'woocommerce_process_shop_order_meta', 'save_order_custom_field_meta_data', 12, 2 );
function save_order_custom_field_meta_data( $post_id, $post ){
    if( isset( $_POST[ 'customer_ref' ] ) ){
        // Save "customer reference" as order meta data
        update_post_meta( $post_id, 'META_FIELD', sanitize_text_field( $_POST[ 'customer_ref' ] ) );

        // Update the existing "customer reference" item meta data
        if( isset( $_POST[ 'item_id_ref' ] ) )
            wc_update_order_item_meta( $_POST[ 'item_id_ref' ], 'Your Reference', $_POST[ 'customer_ref' ] );
    }
}