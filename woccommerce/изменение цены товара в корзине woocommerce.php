<?php 
// functions.php 



add_action( 'woocommerce_before_calculate_totals', 'changing_the_price_of_an_item_in_the_woocommerce_cart' );
 
function changing_the_price_of_an_item_in_the_woocommerce_cart( $cart_object ) {
 
 	 
    //выбираем продукт с конкретным ID и меняем только его
	$product_id = 35; 
 
	// получаем колличество
	foreach ( $cart_object->get_cart() as $cart_id => $cart_item ) {
 
		if( $cart_item[ 'product_id' ] == $product_id ) {
			$quantity = $cart_item[ 'quantity' ];
			break;
		} 
	}
 
	// если количество товара больше трёх, можно задать любое своё значение
	if( ! empty( $quantity ) && $quantity > 3 ) {
 
		// опять цикл, да
		foreach ( $cart_object->get_cart() as $cart_id => $cart_item ) {
 
			// если нужный товар
			if( $cart_item['product_id'] == $product_id ) {
 
				//  скидка 50%
				$newprice = $cart_item['data']->get_regular_price() / 2;
                
                //Устанавливаем новую цену в корзине
				$cart_item['data']->set_price( $newprice );
 
			}
 
		}
	}
 
}

function woocommerce_custom_price_to_cart_item( $cart_object ) {  
    if( !WC()->session->__isset( "reload_checkout" )) {
        foreach ( $cart_object->cart_contents as $key => $value ) {
            if( isset( $value["custom_price"] ) ) { 
                $value['data']->set_price($value["custom_price"]);
            }
            if( isset( $value["custom_price"] ) ) { 
                $value['data']->set_name($value["custom_name"]);
            }
        }  
    }  
}
add_action( 'woocommerce_before_calculate_totals', 'woocommerce_custom_price_to_cart_item', 99 );